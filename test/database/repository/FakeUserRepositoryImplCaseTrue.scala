package database.repository

import api.dtos.CoinInsertionDTO
import api.validators.{ Pass, ValidationResult }

import scala.concurrent.Future

class FakeUserRepositoryImplCaseTrue extends UserRepository {

  def insertCoin(coin: CoinInsertionDTO): Future[ValidationResult] = Future.successful(Pass())
  def isPaid(userId: Int): Future[Boolean] = Future.successful(true)
  def giveChange(userId: Int): Future[Double] = Future.successful(0.0)
  def putUser(userId: Int): Future[ValidationResult] = Future.successful(Pass())
  def isTransactionValid(userId: Int): Future[ValidationResult] = Future.successful(Pass())

}
