package database.repository

import api.dtos.CoinInsertionDTO
import api.validators.{ Fail, ValidationResult }

import scala.concurrent.Future

class FakeUserRepositoryImplCaseFalse extends UserRepository {

  def insertCoin(coin: CoinInsertionDTO): Future[ValidationResult] = Future.successful(Fail(""))
  def isPaid(userId: Int): Future[Boolean] = Future.successful(false)
  def giveChange(userId: Int): Future[Double] = Future.successful(0.0)
  def putUser(userId: Int): Future[ValidationResult] = Future.successful(Fail(""))
  def isTransactionValid(userId: Int): Future[ValidationResult] = Future.successful(Fail(""))

}
