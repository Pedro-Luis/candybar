package database.repository

import api.validators.{ Fail, Pass }
import database.mappings.SlotsMapping.slotTable
import database.mappings.UsersMapping.userTable
import database.properties.TestDBProperty
import definedStrings.Messages.NoStock
import org.scalatest.{ Matchers, _ }
import slick.jdbc.H2Profile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext }

class SlotRepositoryImplTest extends AsyncWordSpec with BeforeAndAfterAll with BeforeAndAfterEach with Matchers {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy private val db: Database = TestDBProperty.db

  private val slotActions: SlotRepositoryImpl = new SlotRepositoryImpl(TestDBProperty)

  private val tables = Seq(slotTable, userTable)

  override def beforeAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.create): _*)), Duration.Inf)

  override def afterAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.drop): _*)), Duration.Inf)

  override def beforeEach(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.delete): _*)), Duration.Inf)

  "SlotRepositoryImpl createFourSlots " should {
    "check if target user can be inserted  " in {
      slotActions.resetsToFourSlots().map(_ shouldBe 4)
    }
  }

  "SlotRepositoryImpl updateStock " should {
    "check if target user can be inserted  " in {
      val result = for {
        _ <- slotActions.resetsToFourSlots()
        updatedStockId1 <- slotActions.updateStock(1)
        updatedStockId2 <- slotActions.updateStock(2)
      } yield (updatedStockId1, updatedStockId2)

      result.map {
        case (updatedStockId1, updatedStockId2) =>
          updatedStockId1 shouldBe 0
          updatedStockId2 shouldBe 0
      }
    }
  }

  "SlotRepositoryImpl hasStock " should {
    "check if target user can be inserted  " in {

      val result = for {
        _ <- slotActions.resetsToFourSlots()
        checkStock1 <- slotActions.hasStock(1)
        _ <- slotActions.updateStock(1)
        checkStockAgain <- slotActions.hasStock(1)
      } yield (checkStock1, checkStockAgain)

      result.map {
        case (checkStock1, checkStockAgain) =>
          checkStock1 shouldBe Pass()
          checkStockAgain shouldBe Fail(NoStock)
      }
    }
  }

}