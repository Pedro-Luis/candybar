package database.repository

import api.validators.{ Fail, ValidationResult }
import definedStrings.Messages.NoStock

import scala.concurrent.Future

class FakeSlotRepositoryImplCaseFalse extends SlotRepository {

  def updateStock(slotId: Int): Future[Int] = Future.successful(-1)
  def resetsToFourSlots(): Future[Int] = Future.successful(0)
  def hasStock(slotId: Int): Future[ValidationResult] = Future.successful(Fail(NoStock))

}

