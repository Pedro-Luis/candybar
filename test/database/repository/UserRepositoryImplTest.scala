package database.repository

import api.dtos.CoinInsertionDTO
import api.validators.{ Fail, Pass }
import database.mappings.SlotsMapping.slotTable
import database.mappings.UsersMapping.userTable
import database.mappings.UsersRow
import database.properties.TestDBProperty
import definedStrings.Messages.{ CoinInsertionFailed, TransactionAborted, UsersFull }
import org.scalatest.{ Matchers, _ }
import slick.jdbc.H2Profile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext, Future }

class UserRepositoryImplTest extends AsyncWordSpec with BeforeAndAfterAll with BeforeAndAfterEach with Matchers {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy private val db: Database = TestDBProperty.db

  private val userActions: UserRepositoryImpl = new UserRepositoryImpl(TestDBProperty, 180000, 5)

  private val tables = Seq(slotTable, userTable)

  override def beforeAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.create): _*)), Duration.Inf)

  override def afterAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.drop): _*)), Duration.Inf)

  override def beforeEach(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.delete): _*)), Duration.Inf)

  val coin = CoinInsertionDTO(1.0, 1)

  "UserRepositoryImpl putUser " should {
    "check if target user can be inserted  " in {
      userActions.putUser(coin.userId).map(_ shouldBe Pass())
    }
  }

  "UserRepositoryImpl putUser " should {
    "check if target user already exists  " in {
      for {
        _ <- userActions.putUser(coin.userId)
        user <- userActions.putUser(coin.userId)
      } yield user shouldBe Pass()
    }
  }

  "UserRepositoryImpl putUser " should {
    "target user shouldn´t be inserted " in {
      val seqUser: Seq[Int] = 1 to 10

      val result = for {
        users <- Future.sequence(seqUser.map(user => userActions.putUser(user)))
      } yield users.fold(Pass())(_.and(_))

      result.map(_ shouldBe Fail(UsersFull))
    }
  }

  "UserRepositoryImpl insertCoin" should {
    "insert correctly a coin if the user exists " in {

      for {
        _ <- userActions.putUser(coin.userId)
        insertCoin <- userActions.insertCoin(coin)
      } yield insertCoin shouldBe Pass()

    }
  }

  "UserRepositoryImpl insertCoin" should {
    "fail to insert if the user doesnt exist" in {
      userActions.insertCoin(coin).map(_ shouldBe Fail(CoinInsertionFailed))
    }
  }

  "UserRepositoryImpl isPaid " should {
    "check if target user has paid: case is paid " in {

      for {
        _ <- userActions.putUser(coin.userId)
        _ <- userActions.insertCoin(coin)
        _ <- userActions.insertCoin(coin)
        _ <- userActions.insertCoin(coin)
        isPaid <- userActions.isPaid(coin.userId)
      } yield isPaid shouldBe true

    }
  }

  "UserRepositoryImpl isPaid " should {
    "check if target user has paid: case is not paid " in {

      for {
        _ <- userActions.putUser(coin.userId)
        isPaid <- userActions.isPaid(coin.userId)
      } yield isPaid shouldBe false

    }
  }

  "UserRepositoryImpl giveChange " should {
    "check if target user has change to receive: case gives 0.2" in {

      for {
        _ <- userActions.putUser(coin.userId)
        _ <- userActions.insertCoin(coin)
        _ <- userActions.insertCoin(coin)
        _ <- userActions.insertCoin(coin)
        change <- userActions.giveChange(coin.userId)
      } yield change shouldBe 0.2

    }
  }

  "UserRepositoryImpl isTransactionValid " should {
    "check if the transaction is under 3 minutes " in {
      for {
        _ <- userActions.putUser(coin.userId)
        isTransactionValid <- userActions.isTransactionValid(coin.userId)
      } yield isTransactionValid shouldBe Pass()
    }
  }

  "UserRepositoryImpl isTransactionValid " should {
    "check if transaction passed 3 min " in {
      Await.result(db.run(userTable += UsersRow(coin.userId, "Pedro", 0.0, System.currentTimeMillis())), Duration.Inf)
      userActions.isTransactionValid(coin.userId).map(_ shouldBe Fail(TransactionAborted))
    }
  }

}