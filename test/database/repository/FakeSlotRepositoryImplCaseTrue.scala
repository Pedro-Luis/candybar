package database.repository

import api.validators.{ Pass, ValidationResult }

import scala.concurrent.Future

class FakeSlotRepositoryImplCaseTrue extends SlotRepository {

  def updateStock(slotId: Int): Future[Int] = Future.successful(0)
  def resetsToFourSlots(): Future[Int] = Future.successful(4)
  def hasStock(slotId: Int): Future[ValidationResult] = Future.successful(Pass())

}
