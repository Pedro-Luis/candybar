package api.controllers

import akka.actor.ActorSystem
import akka.stream.Materializer
import database.repository._
import definedStrings.Messages.CandiesInSlots
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.Mode
import play.api.inject.Injector
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.{ ControllerComponents, Results }
import play.api.test.FakeRequest
import play.api.test.Helpers._

import scala.concurrent.ExecutionContext

class SlotsControllerUnitTest extends PlaySpec with GuiceOneAppPerSuite with BeforeAndAfterAll with BeforeAndAfterEach with Results {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  private val usersActionsCaseTrue: UserRepository = new FakeUserRepositoryImplCaseTrue()
  private val usersActionsCaseFalse: UserRepository = new FakeUserRepositoryImplCaseFalse()

  private val slotsActionsCaseTrue: SlotRepository = new FakeSlotRepositoryImplCaseTrue()
  private val slotsActionsCaseFalse: SlotRepository = new FakeSlotRepositoryImplCaseFalse()

  val appBuilder: GuiceApplicationBuilder = new GuiceApplicationBuilder().in(Mode.Test)
  lazy val injector: Injector = appBuilder.injector()
  lazy val cc: ControllerComponents = injector.instanceOf[ControllerComponents]
  val actorSystem: ActorSystem = injector.instanceOf[ActorSystem]
  lazy implicit val mat: Materializer = injector.instanceOf[Materializer]

  "SlotsControllers #createSlots" should {
    "send a Created if the 4 slots were created" in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseTrue, slotsActionsCaseTrue)
      val result = controller.createSlots.apply(FakeRequest(POST, "/slots")
        .withHeaders(HOST -> "localhost:9000"))

      status(result) mustBe CREATED
      contentAsString(result) mustBe CandiesInSlots
    }
  }

  "SlotsControllers #createSlots" should {
    "send a NotModified if there was an error creating the 4 slots" in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseFalse, slotsActionsCaseFalse)
      val result = controller.createSlots.apply(FakeRequest(POST, "/slots")
        .withHeaders(HOST -> "localhost:9000"))

      status(result) mustBe NOT_MODIFIED
    }
  }

  /*No point in creating 5 more fake classes to test all cases, when they are tested in functional tests */

  "SlotsControllers #InsertCoin" should {
    "send a Ok " in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseTrue, slotsActionsCaseTrue)
      val result = controller.insertCoin(1).apply(FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1))))
      status(result) mustBe OK
    }
  }

  "SlotsControllers #InsertCoin" should {
    "send a BadRequest: case something in user userAction fails" in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseFalse, slotsActionsCaseTrue)
      val result = controller.insertCoin(1).apply(FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1))))
      status(result) mustBe BAD_REQUEST

    }
  }

  "SlotsControllers #InsertCoin" should {
    "send a BadRequest: case something in user slotAction fails" in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseTrue, slotsActionsCaseFalse)
      val result = controller.insertCoin(1).apply(FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1))))
      status(result) mustBe BAD_REQUEST

    }
  }

  "SlotsControllers #InsertCoin" should {
    "send a BadRequest: case slotAction and userAction fails" in {
      val controller = new SlotsController(cc, actorSystem, usersActionsCaseFalse, slotsActionsCaseFalse)
      val result = controller.insertCoin(1).apply(FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1))))
      status(result) mustBe BAD_REQUEST

    }
  }

}