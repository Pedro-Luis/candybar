package api.controllers

import database.mappings.SlotsMapping.slotTable
import database.mappings.UsersMapping.userTable
import database.properties.TestDBProperty
import definedStrings.Messages._
import org.scalatest.{ BeforeAndAfterAll, BeforeAndAfterEach }
import org.scalatestplus.play.PlaySpec
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.libs.json.Json._
import play.api.test.FakeRequest
import play.api.test.Helpers._
import slick.jdbc.H2Profile.api._

import scala.concurrent.duration.Duration
import scala.concurrent.{ Await, ExecutionContext }

class SlotsControllerFunctionalTest extends PlaySpec with GuiceOneAppPerSuite with BeforeAndAfterAll with BeforeAndAfterEach {

  implicit private val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  lazy implicit private val db: Database = TestDBProperty.db

  private val tables = Seq(slotTable, userTable)

  override def beforeAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.create): _*)), Duration.Inf)

  override def afterAll(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.schema.drop): _*)), Duration.Inf)

  override def beforeEach(): Unit = Await.result(db.run(DBIO.seq(tables.map(_.delete): _*)), Duration.Inf)

  "SlotsController functional test" should {
    "use case" in {
      val createSlots = FakeRequest(POST, "/slots")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")

      val slotsResult = route(app, createSlots)
      status(slotsResult.get) mustBe CREATED
      contentAsString(slotsResult.get) mustBe CandiesInSlots

      val insertCoin1 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val coinInserted1Result = route(app, insertCoin1)
      status(coinInserted1Result.get) mustBe OK
      contentAsString(coinInserted1Result.get) mustBe CoinsMissing

      val insertCoin2 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val coinInserted2Result = route(app, insertCoin2)
      status(coinInserted2Result.get) mustBe OK
      contentAsString(coinInserted2Result.get) mustBe CoinsMissing

      val insertCoin3 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val coinInserted3Result = route(app, insertCoin3)
      status(coinInserted3Result.get) mustBe OK
      contentAsString(coinInserted3Result.get) mustBe CandyDelivery + 0.2

      val insertCoin4 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 2)))

      val coinInserted4Result = route(app, insertCoin4)
      status(coinInserted4Result.get) mustBe BAD_REQUEST
      contentAsString(coinInserted4Result.get) mustBe NoStock

      val invalidCoin = FakeRequest(POST, "/slot/2")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 3.0, "userId" -> 2)))

      val invalidCoinResult = route(app, invalidCoin)
      status(invalidCoinResult.get) mustBe BAD_REQUEST

    }
  }

  "SlotsController functional test" should {
    "send a BadRequest if the slots were not created " in {
      val fakeRequest = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val result = route(app, fakeRequest)
      status(result.get) mustBe BAD_REQUEST
      contentAsString(result.get) mustBe NoStock
    }
  }

  "SlotsController functional test" should {
    "send a BadRequest if the users are full " in {
      val createSlots = FakeRequest(POST, "/slots")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")

      val slotsResult = route(app, createSlots)
      status(slotsResult.get) mustBe CREATED
      contentAsString(slotsResult.get) mustBe CandiesInSlots

      val user1 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val user1Result = route(app, user1)
      status(user1Result.get) mustBe OK

      val user2 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 2)))

      val user2Result = route(app, user2)
      status(user2Result.get) mustBe OK

      val user3 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 3)))

      val user3Result = route(app, user3)
      status(user3Result.get) mustBe OK

      val user4 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 4)))

      val user4Result = route(app, user4)
      status(user4Result.get) mustBe OK

      val user5 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 5)))

      val user5Result = route(app, user5)
      status(user5Result.get) mustBe OK

      val user6 = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 6)))

      val user6Result = route(app, user6)
      status(user6Result.get) mustBe BAD_REQUEST
      contentAsString(user6Result.get) mustBe UsersFull

      Thread.sleep(6000)

      val user1TimeOut = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val user1TimeOutResult = route(app, user1TimeOut)
      status(user1TimeOutResult.get) mustBe BAD_REQUEST
      contentAsString(user1TimeOutResult.get) mustBe TransactionAborted

      val user6Again = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 6)))

      val user6AgainResult = route(app, user6Again)
      status(user6AgainResult.get) mustBe OK

    }
  }

  "SlotsController functional test" should {
    "check if transaction abort" in {
      val createSlots = FakeRequest(POST, "/slots")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")

      val slotsResult = route(app, createSlots)
      status(slotsResult.get) mustBe CREATED
      contentAsString(slotsResult.get) mustBe CandiesInSlots

      val insertCoin = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val insertCoinResult = route(app, insertCoin)
      status(insertCoinResult.get) mustBe OK

      Thread.sleep(6000)

      val insertCoinAgain = FakeRequest(POST, "/slot/1")
        .withHeaders(CONTENT_TYPE -> JSON, HOST -> "localhost:9000")
        .withBody(Json.toJson(Json.obj(fields = "coin" -> 1.0, "userId" -> 1)))

      val insertCoinAgainResult = route(app, insertCoinAgain)
      status(insertCoinAgainResult.get) mustBe BAD_REQUEST
      contentAsString(insertCoinAgainResult.get) mustBe TransactionAborted

    }
  }

}
