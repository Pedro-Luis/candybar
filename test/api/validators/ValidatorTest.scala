package api.validators

import org.scalatest.{ AsyncWordSpec, BeforeAndAfterAll, BeforeAndAfterEach, Matchers }

class ValidatorTest extends AsyncWordSpec with BeforeAndAfterAll with BeforeAndAfterEach with Matchers {

  "Validators #and" should {
    "check validators #and logic" in {
      Pass() and Pass() shouldBe Pass()
      Pass() and Fail("") shouldBe Fail("")
      Fail("first") and Fail("") shouldBe Fail("first")
    }
  }

}