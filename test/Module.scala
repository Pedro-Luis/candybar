import com.google.inject.AbstractModule
import database.OnStartUp
import database.properties.{ ProdDBProperty, TestDBProperty }
import database.repository._

import scala.concurrent.ExecutionContext

/** Outline the database to be used implicitly */
class Module extends AbstractModule {

  override def configure(): Unit = {

    lazy val timeOut: Long = 6000
    lazy val maxUsers: Int = 5

    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    bind(classOf[UserRepository]).toInstance(new UserRepositoryImpl(TestDBProperty, timeOut, maxUsers))
    bind(classOf[SlotRepository]).toInstance(new SlotRepositoryImpl(TestDBProperty))

  }
}