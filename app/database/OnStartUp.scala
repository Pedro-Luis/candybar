package database

import database.mappings.SlotsMapping.slotTable
import database.mappings.UsersMapping.userTable
import database.properties.DBProperty
import javax.inject.{ Inject, Singleton }
import slick.jdbc.H2Profile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class OnStartUp @Inject() (ProdDBProperties: DBProperty) {
  val db = ProdDBProperties.db

  val tables = Seq(slotTable, userTable)
  Await.result(db.run(DBIO.seq(tables.map(_.schema.create): _*)), Duration.Inf)

}
