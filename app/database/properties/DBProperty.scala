package database.properties

import slick.jdbc.H2Profile.api._

trait DBProperty {
  val db: Database
}