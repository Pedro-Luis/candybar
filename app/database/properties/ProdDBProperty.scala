package database.properties

import javax.inject.Singleton
import slick.jdbc.H2Profile.api._

@Singleton
object ProdDBProperty extends DBProperty {
  override val db: Database = Database.forURL(
    url = "jdbc:h2:mem:proddb;MODE=MYSQL;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=FALSE;",
    driver = "org.h2.Driver")
}