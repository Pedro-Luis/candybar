package database.mappings

import slick.jdbc.H2Profile.api._

case class UsersRow(
  id: Int,
  username: String,
  totalCoins: Double,
  validTime: Long)

class UserTable(tag: Tag) extends Table[UsersRow](tag, "users") {
  def userId = column[Int]("slotsId")
  def username = column[String]("username")
  def totalCoins = column[Double]("totalCoins")
  def validTime = column[Long]("validTime")

  def * = (userId, username, totalCoins, validTime) <> (UsersRow.tupled, UsersRow.unapply)
}

object UsersMapping {
  lazy val userTable = TableQuery[UserTable]
}