package database.mappings

import slick.jdbc.H2Profile.api._

case class SlotsRow(
  slotId: Int,
  stock: Int)

class SlotTable(tag: Tag) extends Table[SlotsRow](tag, "slots") {
  def slotId = column[Int]("slotsId")
  def stock = column[Int]("stock")

  def * = (slotId, stock) <> (SlotsRow.tupled, SlotsRow.unapply)
}

object SlotsMapping {
  lazy val slotTable = TableQuery[SlotTable]
}