package database.repository

import api.validators.{ Fail, Pass, ValidationResult }
import database.mappings.SlotsMapping.slotTable
import database.mappings.SlotsRow
import database.properties.DBProperty
import definedStrings.Messages.NoStock
import slick.jdbc.H2Profile.api._

import scala.concurrent.{ ExecutionContext, Future }

class SlotRepositoryImpl(ProdDBProperties: DBProperty)(implicit val executionContext: ExecutionContext) extends SlotRepository {
  val db = ProdDBProperties.db

  /** Creates 4 slots corresponding to 4 candies */
  def resetsToFourSlots(): Future[Int] = {

    val result = for {
      _ <- slotTable.delete
      slot1 <- slotTable += SlotsRow(1, 1)
      slot2 <- slotTable += SlotsRow(2, 1)
      slot3 <- slotTable += SlotsRow(3, 1)
      slot4 <- slotTable += SlotsRow(4, 1)
    } yield slot1 + slot2 + slot3 + slot4

    db.run(result.transactionally)

  }

  /**
   * Takes 1 candy of the target stock
   * @param slotId the id of that target slot
   * @return the number of candies in stock from that target slot
   */
  def updateStock(slotId: Int): Future[Int] = {
    val result = for {
      stock <- slotTable.filter(_.slotId === slotId).map(_.stock).result.headOption
      _ <- slotTable.filter(_.slotId === slotId).map(_.stock).update(stock.getOrElse(0) - 1)
    } yield stock.getOrElse(0) - 1
    db.run(result.transactionally)
  }

  /**
   * Checks if target slot has stock
   * @param slotId Takes the candy of the target stock
   * @return Pass if the slot has candies in it, otherwise returns Fail with the target message
   */
  def hasStock(slotId: Int): Future[ValidationResult] = {
    db.run(slotTable.filter(_.slotId === slotId).map(_.stock).result.headOption).map {
      case Some(stock) if stock > 0 => Pass()
      case _ => Fail(NoStock)
    }
  }
}