package database.repository

import api.validators.ValidationResult

import scala.concurrent.Future

trait SlotRepository {
  def updateStock(slotId: Int): Future[Int]
  def resetsToFourSlots(): Future[Int]
  def hasStock(slotId: Int): Future[ValidationResult]
}
