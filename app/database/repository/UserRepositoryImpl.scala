package database.repository

import java.util.UUID.randomUUID

import api.dtos.CoinInsertionDTO
import api.validators._
import database.mappings.UsersMapping._
import database.mappings.UsersRow
import database.properties.DBProperty
import definedStrings.Messages._
import slick.jdbc.H2Profile.api._

import scala.concurrent.{ ExecutionContext, Future }

class UserRepositoryImpl(ProdDBProperties: DBProperty, timeOut: Long, maxUsers: Int)(implicit val executionContext: ExecutionContext) extends UserRepository {

  val db = ProdDBProperties.db

  /**
   * Tries to insert a coin from target user
   * @param coin has the coin value and the userId
   * @return A validation result, that is either a Pass(), or a Fail(message), with the corresponding fail message
   */
  def insertCoin(coin: CoinInsertionDTO): Future[ValidationResult] = {

    for {
      coinsAlreadyInserted <- db.run(userTable.filter(_.userId === coin.userId).map(_.totalCoins).result.headOption)
      coinInsertion <- db.run(userTable.filter(_.userId === coin.userId).map(_.totalCoins).update(coinsAlreadyInserted.getOrElse(0.0) + coin.coin))
    } yield coinInsertion match {
      case 1 => Pass()
      case _ => Fail(CoinInsertionFailed)
    }
  }

  /**
   * Checks if the user has paid the candy
   * @param userId the id of that target user
   * @return True if the user has paid, or false the other case around
   */
  def isPaid(userId: Int): Future[Boolean] = {
    db.run(userTable.filter(_.userId === userId).filter(_.totalCoins >= 2.8).exists.result)
  }

  /**
   * Give the change of a target user that has paid
   * @param userId the id of that target user
   * @return the change
   */
  def giveChange(userId: Int): Future[Double] = {
    val result = for {
      totalCoins <- userTable.filter(_.userId === userId).filter(_.totalCoins >= 2.8).map(_.totalCoins).result.headOption
      _ <- userTable.filter(_.userId === userId).filter(_.totalCoins >= 2.8).delete
    } yield math.floor((totalCoins.getOrElse(0.0) - 2.8) * 100) / 100

    db.run(result.transactionally)
  }

  /**
   * @return the time until the user is timed out
   */
  private[repository] def valid3Min: Long = {
    val currentTime = System.currentTimeMillis()
    currentTime + timeOut
  }

  /**
   * Inserts a user into database if possible
   * @param userId the id of that target user
   * @return Fail if there are too many users in the database, otherwise returns pass
   */
  def putUser(userId: Int): Future[ValidationResult] =
    for {
      userExists <- db.run(userTable.filter(_.userId === userId).exists.result)
      insertPossible <- db.run(userTable.size.result).map(_ < maxUsers)
      _ <- if (!userExists && insertPossible) {
        db.run(userTable += UsersRow(userId, "user" + randomUUID(), 0, valid3Min))
      } else {
        Future.successful(0)
      }
    } yield if (userExists || insertPossible) Pass() else Fail(UsersFull)

  /**
   * Checks if the transaction has timed out
   * @param userId the id of that target user
   * @return Pass if the transaction is on time, otherwise returns Fail with the corresponding message
   */
  def isTransactionValid(userId: Int): Future[ValidationResult] = {
    db.run(userTable.filter(_.userId === userId).filter(_.validTime > System.currentTimeMillis()).exists.result).map {
      case true => Pass()
      case false =>
        deleteUser(userId)
        Fail(TransactionAborted)
    }
  }

  /**
   * Deletes target user from database
   * @param userId the id of that target user
   * @return The number of users that were deleted
   */
  private[repository] def deleteUser(userId: Int): Future[Int] = {
    db.run(userTable.filter(_.userId === userId).delete)
  }

}
