package database.repository

import api.dtos.CoinInsertionDTO
import api.validators.ValidationResult

import scala.concurrent.Future

trait UserRepository {

  def insertCoin(coin: CoinInsertionDTO): Future[ValidationResult]
  def isPaid(userId: Int): Future[Boolean]
  def giveChange(userId: Int): Future[Double]
  def putUser(userId: Int): Future[ValidationResult]
  def isTransactionValid(userId: Int): Future[ValidationResult]

}
