package definedStrings

object Messages {

  val NoStock = "No stock available"
  val CandyDelivery = "Here have your candy, and your change: "
  val CoinsMissing = "Please insert another coin"
  val CandiesInSlots = "Candies are in the slots of the corresponding machine"
  val UsersFull = "No more users allowed"
  val TransactionAborted = "You took to much time, transaction aborted."
  val CoinInsertionFailed = "Couldn´t insert coin"

}
