package api.controllers

import akka.actor.ActorSystem
import api.dtos.CoinInsertionDTO
import api.validators.{ Fail, Pass, ValidationResult }
import database.repository.{ SlotRepository, UserRepository }
import definedStrings.Messages._
import javax.inject._
import play.api.libs.json.{ JsError, JsValue, Json }
import play.api.mvc._

import scala.concurrent.{ ExecutionContext, Future }

/** Class injected with end-points*/

@Singleton class SlotsController @Inject() (cc: ControllerComponents, actorSystem: ActorSystem, userActions: UserRepository, slotActions: SlotRepository)(implicit exec: ExecutionContext) extends AbstractController(cc) {

  def insertCoin(slotId: Int): Action[JsValue] = Action(parse.json).async { request: Request[JsValue] =>

    val coinJson = request.body.validate[CoinInsertionDTO]

    coinJson.fold(

      errors => {
        Future.successful(BadRequest(Json.obj("status" -> "error", "message" -> JsError.toJson(errors))))
      },
      coin => {

        def primaryValidations(coin: CoinInsertionDTO): Future[ValidationResult] =
          for {
            putUser <- userActions.putUser(coin.userId)
            userTimedOut <- userActions.isTransactionValid(coin.userId)
            stock <- slotActions.hasStock(slotId)
          } yield putUser and userTimedOut and stock

        primaryValidations(coin).flatMap {

          case Pass() => userActions.insertCoin(coin).flatMap {

            case Fail(error) => Future.successful(BadRequest(error))
            case _ => userActions.isPaid(coin.userId).flatMap {

              case true => userActions.giveChange(coin.userId).flatMap { change =>

                slotActions.updateStock(slotId).map(_ => Ok(CandyDelivery + change))
              }
              case false => Future.successful(Ok(CoinsMissing))
            }
          }
          case Fail(error) => Future.successful(BadRequest(error))
        }
      })
  }

  def createSlots: Action[AnyContent] = Action.async {
    slotActions.resetsToFourSlots().map {
      case 4 => Created(CandiesInSlots)
      case _ => NotModified
    }
  }

}