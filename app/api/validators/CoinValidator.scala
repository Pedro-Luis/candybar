package api.validators

object CoinValidator {

  val quarter: Double = 0.25
  val halfDollar: Double = 0.50
  val dollar: Double = 1

  val possibleCoins = List(quarter, halfDollar, dollar)
}
