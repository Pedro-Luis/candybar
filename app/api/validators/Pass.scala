package api.validators

case class Pass() extends ValidationResult {
  def and(vr: ValidationResult): ValidationResult = vr match {
    case Fail(_) => vr
    case _ => this
  }
}
