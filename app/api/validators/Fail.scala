package api.validators

case class Fail(error: String) extends ValidationResult {
  def and(vr: ValidationResult): ValidationResult = this
}
