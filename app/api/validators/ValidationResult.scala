package api.validators

trait ValidationResult {
  def and(vr: ValidationResult): ValidationResult
}
