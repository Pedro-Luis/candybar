package api.dtos

import api.validators.CoinValidator
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

case class CoinInsertionDTO(
  coin: Double,
  userId: Int)

object CoinInsertionDTO {
  implicit val coinInsertionDTO: Reads[CoinInsertionDTO] = (
    (__ \ "coin").read[Double](filter[Double](JsonValidationError("Coin is not valid."))(CoinValidator.possibleCoins.contains(_))) and
    (__ \ "userId").read[Int])(CoinInsertionDTO.apply _)
}