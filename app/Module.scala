import com.google.inject.AbstractModule
import com.typesafe.config.ConfigFactory
import database.OnStartUp
import database.properties.ProdDBProperty
import database.repository._
import play.api.Configuration
import play.api.Play

import scala.concurrent.ExecutionContext

/** Outline the database to be used implicitly */
class Module extends AbstractModule {

  override def configure(): Unit = {

    val conf = ConfigFactory.load()
    val timeOut = conf.getLong("timeOut")
    val maxUsers = conf.getInt("maxUsers")

    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    bind(classOf[OnStartUp]).toInstance(new OnStartUp(ProdDBProperty))
    bind(classOf[UserRepository]).toInstance(new UserRepositoryImpl(ProdDBProperty, timeOut, maxUsers))
    bind(classOf[SlotRepository]).toInstance(new SlotRepositoryImpl(ProdDBProperty))

  }
}