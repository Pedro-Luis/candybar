# candybar

Create a Play application that simulates a candy bar vending machine.

Each candy bar costs $2.80 and customers have a combination of $0.25, $0.50 & $1 coins.

The application must expose an API that allows users to purchase a candy bar by submitting a single coin at a time until the candy bar is paid for.
The vending machine has 4 candy bars in stock.
Once a customer starts a transaction, they have to complete it within 3 minutes else the vending machine will abort the transaction and return their money. 
The application should support at most 5 concurrent clients.
Fork and send me a Pull Request with your solution.

--------------------------------------------------------------------------------------------------------

After the slots are created, we will have 4 slots, each with 1 candy in it, so we have 4 candies in stock.
The vending machine is limited to 5 users, and the transaction will abort after 3 minutes, starting with the first coin insertion.
Each coin is associated with a user, so if the user wants to change is mind after he inserted the first coin, he only needs to change the slot, no need to abort the transaction.

How to use:

Post /slots -> to create slots with candies, 1 candy per slot

Post /slot/:id ->with the following Json Body example:
{
    "coin": 1.0,
    "userId": 1
}
to insert a coin into target slot, since we have 4 candies there are only 4 possible ids: [1 to 4]